package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import fr.dawan.springcore.beans.Datasource;

@Configuration // => classe de configuration
@ComponentScan(basePackages = "fr.dawan.springcore") // => scan des classes du package pour trouver les composants
                                                     // @Component, @Repository, @Controller,@Service
public class AppConf {

    // Déclarer un bean une méthode annotée avec @Bean
    // Le type de retour est le type du bean, le nom du bean et le nom de la méthode
    @Bean
    Datasource datasource1() { // On déclare un bean qui a pour nom datasource1 et qui est de type DataSource
        return new Datasource("urlbdd1");
    }

    // L'attribut name de @Bean permet de définir le nom du bean (un ou plusieurs)
    // dans ce cas, le nom de la méthode n'est plus prix en compte
    @Bean(name = "datasource2")
    @Scope("prototype") // Par défaut, la portée est singleton  
    // @Primary => s'il y a plusieurs beans du même type, avec un @Autowired, c'est
    // le bean annoté avec @Primary qui sera sélectionner
    Datasource getDatasource() {
        return new Datasource("urlbdd2");
    }


//  Une dépendance peut être matérialisé avec:
//   - les paramètres de la méthode
//    @Bean
//    FilmRepository filmrepoM1(Datasource datasource1) {
//        return new FilmRepository(datasource1);
//    }

//  - une méthode du bean qui en appelle une autre
//    @Bean
//    FilmRepository filmrepoM2() {
//        return new FilmRepository(datasource1());
//    }

}
