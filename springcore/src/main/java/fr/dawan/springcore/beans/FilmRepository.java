package fr.dawan.springcore.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
// @Named       // -> équivalent à @Component en JavaEE
// @Singleton   // -> équivalent à @Scope("singleton") en JavaEE , par défaut prototype
@Repository("filmrepo") // @Service, @Repository, @Controller , @Composant => un bean est créé
@Lazy   // @Lazy retarde la création d'un singleton à sa première utilisation
public class FilmRepository {
    // Depuis spring 3 les annotations JavaEE (JSR 330) fonctionne avec Spring
    // @Inject                  // -> équivalent à @Autowired en JavaEE
    // @Named("datasource2")    // -> équivalent à @Qualifier en JavaEE
    
    @Autowired
    @Qualifier("datasource2")
    // injection automatique de la dépendence. Un bean de type Datasource est
    // recherché dans le conteneur d'ioc et il est injecté dans la variable d'instance dataSource
    // s'il y a plusieurs bean une exception est générée, à moins de lever l'ambiguité avec @Primary ou @Qualifier
    // @Autowired(required = false)
    // required = false-> dépendence optionnelle: s'il n'y a pas de bean de type
    // Datasource dans le conteneur -> repository= null et pas d'exception
    // @Qualifier -> permet de choisir le bean en fonction de son  nom, s'il y a plusieurs bean du même type
    private Datasource dataSource;

    public FilmRepository() {
        System.out.println("Constructeur par défaut filmrepository");
    }

    // @Autowired
    // @Autowired peut être placer sur :
    // - une variable d'instance: constructeur par défaut et injection en utilisant la reflexion
    // - le setter: constructeur par défaut et injection via le setter
    // - le construteur: injection via les paramètres du constructeur
    //                   @Autowired n'est pas obligatoire, s'il n'y a qu'un seul constructeur
    public FilmRepository(/* @Qualifier("datasource1") */ Datasource dataSource) {
        this.dataSource = dataSource;
    }

    public Datasource getDataSource() {
        return dataSource;
    }

    // @Autowired
    public void setDataSource(/* @Qualifier("datasource1") */ Datasource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public String toString() {
        return "FilmRepository [dataSource=" + dataSource + ", toString()=" + super.toString() + "]";
    }

}
