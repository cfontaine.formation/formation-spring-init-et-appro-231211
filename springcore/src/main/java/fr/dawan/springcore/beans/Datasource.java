package fr.dawan.springcore.beans;

import java.io.Serializable;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)

public class Datasource implements Serializable {

    private static final long serialVersionUID = 1L;

    private String urlBdd;
    
    // @PostConstruct, @PreDestroy -> il faut ajouter la dépendance: jakarta.annotation-api

    // @PostConstruct ->    La méthode annotée est appelée juste après l'instanciation du bean
    //                      On peut aussi utiliser l'attribut initMethode de @Bean à la place
    @PostConstruct
    public void init() {
        System.out.println("Méthode initalisation");
    }
    
    // @PreDestroy ->  -> La méthode annotée est appelée juste avant la destruction du bean
    //                      On peut aussi utiliser l'attribut destroyMethode de @Bean à la place
    @PreDestroy
    public void destroy() {
        System.out.println("Méthode destruction");
    }
}
