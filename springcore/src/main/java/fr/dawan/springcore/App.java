package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.Datasource;
import fr.dawan.springcore.beans.FilmRepository;

public class App {
    public static void main(String[] args) {
        // Création du conteneur d'ioc
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConf.class);
        System.out.println("-------------------------------------------------------");
        
        // getBean -> permet de récupérer les instances des beans depuis le conteneur
        System.out.println(ctx.getBean("datasource1", Datasource.class));

        System.out.println(ctx.getBean("datasource2", Datasource.class));

        System.out.println(ctx.getBean("filmrepo", FilmRepository.class));
        
        // Singleton (par défaut)
        // chaque appel à getBean, on obtient le même bean 
        System.out.println(ctx.getBean("datasource1", Datasource.class));
        System.out.println(ctx.getBean("datasource1", Datasource.class));

        // Prototype 
        // chaque appel à getBean crée un nouveau bean
        System.out.println(ctx.getBean("datasource2", Datasource.class));
        System.out.println(ctx.getBean("datasource2", Datasource.class));
        
        System.out.println("-------------------------------------------------------");
        // Fermeture du context entraine la destruction de tous les beans
        ((AbstractApplicationContext) ctx).close();
    }
}
