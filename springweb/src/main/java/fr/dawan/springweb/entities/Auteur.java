package fr.dawan.springweb.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="auteurs")
public class Auteur implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(length = 50, nullable=false)
    private String prenom;
    
    @Column(length = 50, nullable=false)
    private String nom;
    
    @Column(nullable=false)
    private LocalDate naissance;
    
    private LocalDate deces;
    
    @ManyToMany
    private Set<Livre> livres=new HashSet<>();

    public Auteur(String prenom, String nom, LocalDate naissance, LocalDate deces) {
        this.prenom = prenom;
        this.nom = nom;
        this.naissance = naissance;
        this.deces = deces;
    } 
}
