package fr.dawan.springweb.entities;

import java.io.Serializable;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="livres")
public class Livre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;
    
    @Column(length = 240, nullable=false)
    private String titre;
    
    @Column(nullable=false)
    private int annee;
    
    @Column(nullable=false)
    private double prix;
    
    @Lob
    @Column(length = 65000)
    private byte[] couverture;
    
    @Column(length = 550)
    private String resume;
    
    @ManyToOne
    @JoinColumn(name = "genre")
    private Genre genre;
    
    @ManyToMany(mappedBy="livres")
    private Set<Auteur> auteurs=new HashSet<>();
    
    public String generateBase64Image() {
        return Base64.getEncoder().encodeToString(couverture);
    }

    public Livre(String titre, int annee, double prix, String resume) {
        this.titre = titre;
        this.annee = annee;
        this.prix = prix;
        this.resume = resume;
    }
 
}
