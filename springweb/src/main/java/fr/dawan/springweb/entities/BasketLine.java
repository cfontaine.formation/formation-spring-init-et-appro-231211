package fr.dawan.springweb.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class BasketLine {
    
    private Livre livre;
    
    private int quantity;

    public void incQuantity() {
        quantity++;
    }
    
    public void decQuantity() {
        quantity--;
    }
    
    public double total() {
        return livre.getPrix()*quantity;
    }
}
