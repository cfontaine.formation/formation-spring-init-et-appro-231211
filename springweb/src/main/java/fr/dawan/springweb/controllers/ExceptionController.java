package fr.dawan.springweb.controllers;

import java.sql.SQLException;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(SQLException.class)
    public String gestionnaireException(SQLException e, Model model ) {
        model.addAttribute("msgEx", e.getMessage());
        model.addAttribute("traceEx", e.getStackTrace());
        return "exception";
    }
    
}
