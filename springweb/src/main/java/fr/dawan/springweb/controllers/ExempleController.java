package fr.dawan.springweb.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.dawan.springweb.entities.Personne;
import fr.dawan.springweb.services.EmailService;
import jakarta.inject.Provider;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@Controller
@RequestMapping("/exemple")
@SessionAttributes("personne2") // Méthode Session 2
public class ExempleController {

    @Autowired
    private EmailService emailService;

    @Autowired
    private Provider<Personne> provider;    // Méthode Session 3

    private static int compteur;

    // @RequestMapping => utilisée pour mapper les requêtes HTTP aux méthodes du contrôleur
    // Model=> utilisé pour transférer des données entre la vue et le contrôleur
    @RequestMapping(value = { "/testmodel", "/model" }, method = { RequestMethod.GET, RequestMethod.POST })
    public String testModel(Model model) {
        String message = "Test Model";
        model.addAttribute("msg", message);
        return "exemple";
    }

    // ModelAndView => permet de transmettre le nom de la vue et les attributs avec
    // un seul return
    @GetMapping("/testmodelandview")
    public ModelAndView testModelAndView() {
        // On peut passer le nom de la vue au constructeur
        ModelAndView mdv = new ModelAndView(/* "exemple" */);
        mdv.addObject("msg", "Test ModelAndView");
        mdv.setViewName("exemple"); // ou utiliser la méthode setViewName
        return mdv;
    }

    // params -> la méthode sera éxécutée:
    // - si la requête a pour url /exemple/testparams
    // - et si la requéte a un paramètre id
    // (http://localhost:8080/exemple/testparams?id=4)
    @GetMapping(value = "/testparams", params = "id")
    public String testParams(Model model) {
        model.addAttribute("msg", "La requête contient la paramètre id");
        return "exemple";
    }

    // params -> ici en plus la valeur id doit être de 42
    // (http://localhost:8080/exemple/testparamsval?id=42)
    @GetMapping(value = "/testparamsval", params = "id=42")
    public String testParamsVal(Model model) {
        model.addAttribute("msg", "La requête contient la paramètre id = 42");
        return "exemple";
    }

    // produces -> la méthode sera éxécutée:
    // - si la requête a pour url /exemple/testproduces
    // - et si l'en-tête Accept contient text/plain
    @GetMapping(value = "/testproduces", produces = MediaType.TEXT_PLAIN_VALUE)
    public String testProduces(Model model) {
        model.addAttribute("msg", "Le client HTTP accepte le type mime text/plain ");
        return "exemple";
    }

    // @PathVariable
    @GetMapping("/testpath/{id}")
    public String testPath(@PathVariable("id") String idVal, Model model) {
        model.addAttribute("msg", "L'url contient un paramètre id= " + idVal);
        return "exemple";
    }

    @GetMapping("/testpathimpl/{id}")
    public String testPathImpl(@PathVariable String id, Model model) {
        model.addAttribute("msg", "L'url contient un paramètre id= " + id);
        return "exemple";
    }

    @GetMapping("/testpathmulti/{id}/action/{action}")
    public String testPathmulti(@PathVariable String id, @PathVariable String action, Model model) {
        model.addAttribute("msg", "L'url contient les paramètres id= " + id + " et action=" + action);
        return "exemple";
    }

    @GetMapping("/testpathmap/{id}/action/{action}")
    public String testPathmap(@PathVariable Map<String, String> m, Model model) {
        model.addAttribute("msg", "L'url contient les paramètres id= " + m.get("id") + " et action=" + m.get("action"));
        return "exemple";
    }

    // @PathVariable => on peut lever les ambiguités en utilisant des expressions
    // régulières
    // id-> uniquement un nombre entier positif
    @GetMapping("/testpathamb/{id:[0-9]+}")
    public String testPathAmbigue1(@PathVariable String id, Model model) {
        model.addAttribute("msg", "L'url contient un paramètre id= " + id);
        return "exemple";
    }

    // name -> uniquement 1er caractère une lettre ensuite lettre, chiffre ou _
    @GetMapping("/testpathamb/{nom:^\\D[\\w]+}")
    public String testPathAmbigue2(@PathVariable String nom, Model model) {
        model.addAttribute("msg", "L'url contient un paramètre nom= " + nom);
        return "exemple";
    }

    // required = false -> pour rendre @PathVariable optionnel
    @GetMapping({ "/testpathoption/{id}", "/testpathoption" })
    public String testPathOptionnel(@PathVariable(required = false) String id, Model model) {
        if (id == null)
            model.addAttribute("msg", "L'url ne contient pas de paramètre id");
        else {
            model.addAttribute("msg", "L'url contient un paramètre id= " + id);
        }
        return "exemple";
    }

    // RequestParam
    @GetMapping("/testparam")
    public String testParam(@RequestParam("id") String idVal, Model model) {
        model.addAttribute("msg", "paramètre de requête id= " + idVal);
        return "exemple";
    }

    @GetMapping("/testparamimpl")
    public String testParamImpl(@RequestParam String id, Model model) {
        model.addAttribute("msg", "paramètre de requête id= " + id);
        return "exemple";
    }

    @GetMapping("/testparammulti")
    public String testParamMulti(@RequestParam String id, @RequestParam String nom, Model model) {
        model.addAttribute("msg", "paramètre de requête id= " + id + " nom=" + nom);
        return "exemple";
    }

    // @RequestParam => on peut lever les ambiguités en utilisant l'attribut params
    // on peut entrer dans la méthode uniquement, si la requête contient un
    // paramètre id
    @GetMapping(value = "/testparamamb", params = "id")
    public String testParamAmbigue1(@RequestParam String id, Model model) {
        model.addAttribute("msg", "paramètre de requête id= " + id);
        return "exemple";
    }

    // on peut entrer dans la méthode uniquement, si la requête contient un
    // paramètre nom
    @GetMapping(value = "/testparamamb", params = "nom")
    public String testParamAmbigue2(@RequestParam String nom, Model model) {
        model.addAttribute("msg", "paramètre de requête nom= " + nom);
        return "exemple";
    }

    // @RequestParam => l'attribut defaultValue permet de définir une valeur par
    // défaut pour le paramétre
    @GetMapping("/testparamdefault")
    public String testParamDefault(@RequestParam(defaultValue = "42") String id, Model model) {
        model.addAttribute("msg", "paramètre de requête id= " + id);
        return "exemple";
    }

    // @RequestParam => l'attribut required permet de rendre le paramètre optionnel
    @GetMapping("/testparamoption")
    public String testParamOption(@RequestParam(required = false) String id, Model model) {
        model.addAttribute("msg", "paramètre de requête id= " + id);
        return "exemple";
    }

    @GetMapping
    public String exemple() {
        return "exemple";
    }

    @PostMapping("/testformulaire")
    public String testParamFormulaire(@RequestParam String nom, Model model) {
        model.addAttribute("msg", nom);
        return "exemple";
    }

    @GetMapping("/testparamconv")
    public String testParamConv(@RequestParam int id, Model model) {
        model.addAttribute("msg", id);
        return "exemple";
    }

    @GetMapping("/testparamdate")
    public String testParamDate(@DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam LocalDate date, Model model) {
        model.addAttribute("msg", date.toString());
        return "exemple";
    }

    // Lier les JavaBeans
    @GetMapping(value = "/testbindpath/{id}/{prenom}/{nom}")
    public String testBindPath(Personne p, Model model) {
        model.addAttribute("msg", p.toString());
        return "exemple";
    }

    @GetMapping("/testbindparam")
    public String testBindParam(Personne p, Model model) {
        model.addAttribute("msg", p.toString());
        return "exemple";
    }

    // Exemple thymeleaf
    @GetMapping({ "/testhymeleaf", "/testhymeleaf/{val}" })
    public String testThymeleaf(@PathVariable(required = false) Integer val, Model model) {
        model.addAttribute("i", 34);

        Personne p = new Personne(3, "john", "doe");
        model.addAttribute("per", p);

        double tab[] = { 1.23, 4.56, 6.8, 8.9 };
        model.addAttribute("tab", tab);

        Map<String, Integer> m = new HashMap<>();
        m.put("john", 34);
        m.put("jane", 28);
        m.put("alan", 45);
        model.addAttribute("m", m);

        model.addAttribute("val", val);

        List<Personne> lst = new ArrayList<>();
        lst.add(new Personne(1, "john", "doe"));
        lst.add(new Personne(2, "jane", "doe"));
        lst.add(new Personne(3, "Alan", "Smithee"));
        lst.add(new Personne(4, "Jo", "Dalton"));
        lst.add(new Personne(5, "Yves", "Roulo"));

        model.addAttribute("personnes", lst);

        model.addAttribute("html", "<b>un texte</b>");
        model.addAttribute("text", "il pleut aujourd\'hui");
        return "exemplethymleaf";
    }

    // Request Header
    @GetMapping("/testheader")
    public String testHeader(@RequestHeader("user-agent") String userAgent, Model model) {
        model.addAttribute("msg", userAgent);
        return "exemple";
    }

    @GetMapping("/testallheader")
    public String testAllHeader(@RequestHeader HttpHeaders headers, Model model) {
        List<String> lstHeader = new ArrayList<>();
        Set<Entry<String, List<String>>> entry = headers.entrySet();
        for (Entry<String, List<String>> e : entry) {
            String tmp = e.getKey() + " :";
            for (String v : e.getValue()) {
                tmp += v + " ";
            }
            lstHeader.add(tmp);
        }
        model.addAttribute("lstHeader", lstHeader);
        return "Exemple";
    }

    // Redirection
    @GetMapping("/testredirect")
    public String testRedirect() {
        return "redirect:/hello"; // 302
    }

    @GetMapping("/testforward")
    public String testForward() {
        return "forward:/hello";
    }

    // @ModelAttribute sur une méthode
//    @ModelAttribute
//    public void testModelAttribute(Model model) {
//        model.addAttribute("compteur", compteur++);
//    }

    @ModelAttribute("compteur")
    public int testModelAttribute() {
        return compteur++;
    }

    // @ModelAttribute sur un paramètre d'une méthode d'un controller
    @GetMapping("/testmodelattrparam")
    public String testModelAttributeParam(@ModelAttribute("pr1") Personne personne, Model model) {
        model.addAttribute("msg", personne.toString());
        return "exemple";
    }

    @ModelAttribute("pr1")
    public Personne initPersonnePr1() {
        return new Personne(15L, "John", "Doe");
    }

    // FlashAttribute
    @GetMapping("/testflash")
    public String testFlashAttribute(RedirectAttributes rAtt) {
        rAtt.addFlashAttribute("messageFlash", "Vous venez d'être redirigé");
        return "redirect:/exemple/cibleflash";
    }

    @GetMapping("/cibleflash")
    public String cibleFlash(@ModelAttribute("messageFlash") String msgFlash, Model model) {
        model.addAttribute("msg", msgFlash);
        return "exemple";
    }

    // Gestion des exception
    @GetMapping("/ioexception")
    public void genIOException() throws IOException {
        throw new IOException("IOException");
    }

    @GetMapping("/sqlexception")
    public void genSQLException() throws SQLException {
        throw new SQLException("SQLException");
    }

    @ExceptionHandler(IOException.class)
    public String gestionnaireIOException(Exception e, Model model) {
        model.addAttribute("msgEx", e.getMessage());
        model.addAttribute("traceEx", e.getStackTrace());
        return "exception";
    }

    // Upload
    @PostMapping("/upload")
    public String uploadFile(@RequestParam("upload-file") MultipartFile file, Model model) throws IOException {
        String infoFile = file.getOriginalFilename() + " " + file.getContentType() + " " + file.getSize();
        model.addAttribute("msg", "Upload: " + infoFile);
        try (BufferedOutputStream bow = new BufferedOutputStream(
                new FileOutputStream("c:/Dawan/Formations/" + file.getOriginalFilename()))) {
            bow.write(file.getBytes());
            bow.flush();
        }
//        catch(IOException e) {
//            
//        }
        return "exemple";
    }

    @GetMapping("/testsimplemail")
    public String testSimpleMail() {
        emailService.sendSimpleMail("Le message du mail", "test simple mail", "jdoe@dawan.com", "javerange@dawan.com");
        return "redirect:/exemple";
    }

    @GetMapping("/testhtmlmail")
    public String testHTMLMail() {
        String msgHtml = "<html><body><h1>le message de mail en HTML</h1></body></html>";
        emailService.sendHTMLMail(msgHtml, "test html mail", "jdoe@dawan.com", "no-reply@dawan.com");
        return "redirect:/exemple";
    }

    @GetMapping("/testtemplatemail")
    public String testTemplateMail() throws Exception {
        Map<String, Object> model = new HashMap<>();
        model.put("prenom", "John");
        model.put("email", "jdoe@dawan.com");
        emailService.sendTempleMail("mailTemplate.ftlh", model, "un test avec un moteur de template", "jdoe@dawan.com","no-reply@dawan.com");
        return "redirect:/exemple";
    }

    @GetMapping("/testmailattachement")
    public String testMailAttachement() {

        emailService.sendMailAttachement("Le message du mail", "test simple mail", "jdoe@dawan.com",
                "javerange@dawan.com", new File("logo.png"));
        return "redirect:/exemple";
    }

    // Cookie
    @GetMapping("/writecookie")
    public String writeCookie(HttpServletResponse response) {
        Cookie c = new Cookie("testCookie", "valeur_de_test");
        c.setMaxAge(30);
        response.addCookie(c);
        return "redirect:/exemple/readcookie";
    }

    @GetMapping("/readcookie")
    public String readCookie(@CookieValue(value = "testCookie", defaultValue = "cookie : default value") String value,
            Model model) {
        model.addAttribute("msg", "Cookie testCookie=" + value);
        return "exemple";
    }

    // Session
    // Méthode 1: Session JavaEE
    @GetMapping("/writesession1")
    public String writeSession1(HttpServletRequest request, Model model) {
        HttpSession session = request.getSession();
        session.setAttribute("personne1", new Personne(3L, "Alan", "Smithee"));
        model.addAttribute("msg", "Ajout d'un utilisateur dans la session 1");
        return "exemple";
    }

    @GetMapping("/readsession1")
    public String readSession1(HttpServletRequest request, Model model) {
        HttpSession session = request.getSession();
        Personne p = (Personne) session.getAttribute("personne1");
        if (p != null) {
            model.addAttribute("msg", p);
        }
        return "exemple";
    }

    // Méthode 2: @SessionAttributes
    @GetMapping("/writesession2")
    public String writeSession2(Model model) {
        Personne p = new Personne(4L, "Jane", "Doe");
        model.addAttribute("personne2", p);
        model.addAttribute("msg", "Ajout d'un utilisateur dans la session 2");
        return "exemple";
    }

    @GetMapping("/readsession2")
    public String readSession2(@ModelAttribute("personne2") Personne personne, Model model) {
        model.addAttribute("msg", personne);
        return "exemple";
    }

    @ModelAttribute("personne2")
    public Personne initSession2() {
        return new Personne();
    }

    // Méthode 3 spring bean scope session
    @GetMapping("/writesession3")
    public String writeSession3(Model model) {
        Personne personne3 = provider.get();
        personne3.setPrenom("John");
        personne3.setNom("Doe");
        model.addAttribute("msg", "Ajout Personne Session Méthode3");
        return "exemple";
    }

    @GetMapping("/readsession3")
    public String readSession3(Model model) {
        Personne personne3 = provider.get();
          model.addAttribute("msg", "Sesion3" + personne3.getPrenom() + " " + personne3.getNom());
        return "exemple";
    }
    
}