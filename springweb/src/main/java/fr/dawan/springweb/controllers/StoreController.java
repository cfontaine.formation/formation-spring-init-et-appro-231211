package fr.dawan.springweb.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import fr.dawan.springweb.entities.BasketLine;
import fr.dawan.springweb.entities.Livre;
import fr.dawan.springweb.services.LivreService;

@Controller
@RequestMapping("/store")
@SessionAttributes("panier")
public class StoreController {

    @Autowired
    private LivreService livreService;
    
    @GetMapping
    public String getAllLivre(Model model) {
        model.addAttribute("livres",livreService.getAll(Pageable.unpaged()));
        return "store";
    }
    
    @GetMapping("/basket")
    public String getBasket(@ModelAttribute("panier") List<BasketLine> panier, Model model) {
        model.addAttribute("panier", panier);
        return "basket";
    }

    @GetMapping("/basketadd/{id}")
    public String addBasket(@ModelAttribute("panier") List<BasketLine> panier, @PathVariable int id, Model model) {
        BasketLine bl=findBasketLine(id, panier);
        if(bl!=null) {
            bl.incQuantity();
        }
        else {
            Livre livre = livreService.findById(id);
            panier.add(new BasketLine(livre, 1));
        }
        return "redirect:/store";
    }

    @GetMapping("/basketremove/{id}")
    public String deleteBasket(@PathVariable long id, @ModelAttribute("panier") List<BasketLine> panier) {
        BasketLine bl=findBasketLine(id, panier);
        if(bl!=null) {
            panier.remove(bl);
        }
        return "redirect:/store/basket";
    }

    @GetMapping("/basketinc/{id}")
    public String incQuantityBasketLine(@PathVariable long id, @ModelAttribute("panier") List<BasketLine> panier) {
        BasketLine bl=findBasketLine(id, panier);
        if(bl!=null) {
            bl.incQuantity();
        }
        return "redirect:/store/basket";
    }

    @GetMapping("/basketdec/{id}")
    public String decQuantityBasketLine(@PathVariable long id, @ModelAttribute("panier") List<BasketLine> panier) {
        BasketLine bl=findBasketLine(id, panier);
        if(bl!=null) {
            bl.decQuantity();
            if(bl.getQuantity()<=0) {
                panier.remove(bl);
            }
        }
        return "redirect:/store/basket";
    }
    
    @ModelAttribute("panier")
    public List<BasketLine> initPanier() {
        return new ArrayList<>();
    }
    
    
    private BasketLine findBasketLine(long id,List<BasketLine> panier) {
        for (BasketLine b : panier) {
            if (b.getLivre().getId() == id) {
               return b;
            }
        }
        return null;
    }

}
