package fr.dawan.springweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @GetMapping(value = "/login", params = "failed")
    public String loginFailed(Model model) {
        model.addAttribute("msgLogin", "Email ou mot de passe invalide");
        model.addAttribute("error", true);
        return "login";
    }

    @GetMapping(value = "/login", params = "logout")
    public String loginLogout(Model model) {
        model.addAttribute("msgLogin", "Vous êtes déconnecté");
        model.addAttribute("error", false);
        return "login";
    }

}
