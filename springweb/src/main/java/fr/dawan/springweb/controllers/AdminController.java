package fr.dawan.springweb.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.dawan.springweb.entities.Auteur;
import fr.dawan.springweb.entities.Livre;
import fr.dawan.springweb.entities.User;
import fr.dawan.springweb.forms.FormAuteur;
import fr.dawan.springweb.forms.FormLivre;
import fr.dawan.springweb.forms.FormUser;
import fr.dawan.springweb.forms.UserValidator;
import fr.dawan.springweb.services.AuteurService;
import fr.dawan.springweb.services.LivreService;
import fr.dawan.springweb.services.UserService;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private LivreService livreService;

    @Autowired
    private AuteurService auteurService;

    @Autowired
    private UserService userService;
    
    @Autowired
    private PasswordEncoder encoder;

    @GetMapping("/livres")
    public String displayLivre(@ModelAttribute("message") String message, Model model) {
        model.addAttribute("livres", livreService.getAll(Pageable.unpaged()));
        model.addAttribute("message", message);
        return "livres";
    }

    @GetMapping("/livres/delete/{id}")
    public String deleteLivre(@PathVariable long id, RedirectAttributes ra) {
        if (livreService.deleteById(id)) {
            ra.addFlashAttribute("message", "Le livre avec l'id=" + id + " a été supprimé");
        } else {
            ra.addFlashAttribute("message", "Le livre avec l'id=" + id + " n'existe pas");
        }
        return "redirect:/admin/livres";
    }

    @GetMapping("/livres/add")
    public String addLivre(@ModelAttribute("formlivre") FormLivre formLivre) {
        return "ajoutLivre";
    }

    @PostMapping(value = "/livres/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ModelAndView addLivrePost(@Valid @ModelAttribute("formlivre") FormLivre formLivre, BindingResult results) {
        ModelAndView mdv = new ModelAndView();
        if (results.hasErrors()) {
            mdv.addObject("formlivre", formLivre);
            mdv.addObject("errors", results);
            mdv.setViewName("ajoutLivre");
        } else {
            Livre livre = new Livre(formLivre.getTitre(), formLivre.getAnnee(), formLivre.getPrix(),
                    formLivre.getResume());
            try {
                livre.setCouverture(formLivre.getCouverture().getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
            livreService.saveOrUpdate(livre);
            mdv.setViewName("redirect:/admin/livres");
        }
        return mdv;
    }

    @GetMapping("/livres/export")
    public void exportLivreCsv(HttpServletResponse response) {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment;filename=livres.csv");
        try {
            PrintWriter writer = response.getWriter();
            writer.print("id;titre;annee_sortie;prix\n");
            livreService.getAll(Pageable.unpaged()).forEach(
                    l -> writer.print(l.getId() + ";" + l.getTitre() + ";" + l.getAnnee() + ";" + l.getPrix() + "\n"));
            writer.close();
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    @GetMapping("/auteurs")
    public String displayAuteur(Model model) {
        model.addAttribute("auteurs", auteurService.getAll());
        return "auteurs";
    }

    @GetMapping("/auteurs/add")
    public String addAuteur(@ModelAttribute("formauteur") FormAuteur formAuteur) {
        return "ajoutauteur";
    }

    @PostMapping("/auteurs/add")
    public ModelAndView addAuteurPost(@Valid @ModelAttribute("formauteur") FormAuteur formAuteur,
            BindingResult results) {
        ModelAndView mdv = new ModelAndView();
        if (results.hasErrors()) {
            mdv.addObject("formauteur", formAuteur);
            mdv.addObject("errors", results);
            mdv.setViewName("ajoutauteur");
        } else {
            Auteur auteur = new Auteur(formAuteur.getPrenom(), formAuteur.getNom(), formAuteur.getNaissance(),
                    formAuteur.getDeces());
            auteurService.saveOrUpdate(auteur);
            mdv.setViewName("redirect:/admin/auteurs");
        }
        return mdv;
    }

    @GetMapping("/users")
    public String displayUsers(Model model) {
        model.addAttribute("users", userService.getAll());
        return "users";
    }
    
    @GetMapping("/users/add")
    public String addUser(@ModelAttribute("formuser") FormUser formuser) {
        return "ajoutuser";
    }

    @PostMapping("/users/add")
    public ModelAndView addUserPost(@ModelAttribute("formuser") FormUser formuser, BindingResult results) {
        new UserValidator().validate(formuser, results);
        ModelAndView mdv = new ModelAndView();
        if (results.hasErrors()) {
            mdv.setViewName("ajoutuser");
            mdv.addObject("formuser", formuser);
            mdv.addObject("errors", results);
        } else {
            User u = new User(formuser.getUserName(), encoder.encode(formuser.getPassword()));
            userService.saveOrUpdate(u);
            mdv.setViewName("redirect:/exemple");
        }
        return mdv;
    }
}
