package fr.dawan.springweb.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springweb.entities.Auteur;

public interface AuteurRepository extends JpaRepository<Auteur, Long> {

}
