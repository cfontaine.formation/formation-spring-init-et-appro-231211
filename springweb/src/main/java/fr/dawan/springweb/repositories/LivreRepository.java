package fr.dawan.springweb.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springweb.entities.Livre;

public interface LivreRepository extends JpaRepository<Livre, Long> {

}
