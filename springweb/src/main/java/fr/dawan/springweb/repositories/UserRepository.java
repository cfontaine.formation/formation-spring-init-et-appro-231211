package fr.dawan.springweb.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;

import fr.dawan.springweb.entities.User;

public interface UserRepository extends JpaRepository<User, Long>{

    Optional<UserDetails> findByUsername(String username);
}
