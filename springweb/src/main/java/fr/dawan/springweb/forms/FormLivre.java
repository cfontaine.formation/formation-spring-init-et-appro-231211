package fr.dawan.springweb.forms;

import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class FormLivre {
    
    @NotBlank
    @Size(min=1, max=240,message = "Le titre du livre doit faire entre 1 et 240 caractères")
    private String titre;
    
    @PositiveOrZero
    @NotNull
    private int annee;
    
    @NotNull
    @Positive
    private double prix;
    
    @Size(max=550)
    private String resume;
    
    private MultipartFile couverture;
}
