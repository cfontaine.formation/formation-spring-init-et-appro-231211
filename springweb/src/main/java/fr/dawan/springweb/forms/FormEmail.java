package fr.dawan.springweb.forms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FormEmail {

    private String titre;

    private String to;

    private String from;

    private String text;

}
