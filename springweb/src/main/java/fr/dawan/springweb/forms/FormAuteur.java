package fr.dawan.springweb.forms;

import java.time.LocalDate;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FormAuteur {

    @NotBlank
    @Size(min=2,max=50)
    private String prenom;
    
    @NotBlank
    @Size(min=2,max=50)
    private String nom;
    
    @NotNull
    @Past
    private LocalDate naissance;
    
    @PastOrPresent
    private LocalDate deces;
}
