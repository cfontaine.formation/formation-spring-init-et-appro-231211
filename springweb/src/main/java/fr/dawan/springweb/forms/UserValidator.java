package fr.dawan.springweb.forms;

import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == FormUser.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        FormUser formUser = (FormUser) target;
        if (formUser.getPassword().isBlank()) {
            errors.rejectValue("password", "user.password.NotNull",
                    "Le mot de passe ne doit pas être vide");
        }
        if (formUser.getConfirmPassword().isBlank()) {
            errors.rejectValue("confirmPassword", "user.confirmpassWord.NotNull",
                    "La confirmation du mot de passe ne doit pas être vide");
        }
        if(!formUser.getPassword().equals(formUser.getConfirmPassword())) {

            errors.rejectValue("password", "user.passWord.NotEquals","Le mot de passe est différent de la confirmation");
        }
        System.out.println(formUser.getPassword() + " " +  formUser.getConfirmPassword());
        ValidationUtils.rejectIfEmpty(errors, "userName", "user.username.empty", "L'email ne peut pas être vide");

        if (!Pattern.matches(
                "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$",
                formUser.getUserName())) {
            errors.rejectValue("userName", "user.username.notEmail", "L'email n'est pas valide");
        }
    }

}
