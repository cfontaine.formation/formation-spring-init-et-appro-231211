package fr.dawan.springweb.services;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import fr.dawan.springweb.entities.User;

public interface UserService extends UserDetailsService{
    
    List<User> getAll();
    
    User saveOrUpdate(User user); 
}
