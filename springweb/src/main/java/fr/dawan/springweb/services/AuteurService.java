package fr.dawan.springweb.services;

import java.util.List;

import fr.dawan.springweb.entities.Auteur;

public interface AuteurService {

    List<Auteur> getAll();

    Auteur saveOrUpdate(Auteur auteur);
}
