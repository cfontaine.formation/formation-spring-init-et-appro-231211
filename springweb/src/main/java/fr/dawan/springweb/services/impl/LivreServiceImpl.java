package fr.dawan.springweb.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springweb.entities.Auteur;
import fr.dawan.springweb.entities.Livre;
import fr.dawan.springweb.repositories.LivreRepository;
import fr.dawan.springweb.services.LivreService;

@Service
@Transactional
public class LivreServiceImpl implements LivreService {

    @Autowired
    private LivreRepository repository;

    @Override
    public List<Livre> getAll(Pageable page) {
        return repository.findAll(page).getContent();
    }

    @Override
    public boolean deleteById(long id) {
//        Optional<Livre> optLivre=repository.findById(id);
//        if(optLivre.isPresent()) {
//            Livre l=optLivre.get();
//            for(Auteur a: l.getAuteurs()) {
//                a.getLivres().remove(l);
//            }
//            l.getAuteurs().clear();
//            repository.deleteById(id);
//            return true;
//        }
       repository.findById(id).ifPresent(l -> {
            l.getAuteurs().forEach(a -> a.getLivres().remove(l));
            l.getAuteurs().clear();
            repository.deleteById(id);
        });
        return true;  
    }

    @Override
    public Livre saveOrUpdate(Livre livre) {
        return repository.saveAndFlush(livre);
    }

    @Override
    public Livre findById(long id) {
        return repository.findById(id).get();
    }

}
