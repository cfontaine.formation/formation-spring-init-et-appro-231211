package fr.dawan.springweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springweb.entities.Auteur;
import fr.dawan.springweb.repositories.AuteurRepository;
import fr.dawan.springweb.services.AuteurService;

@Service
@Transactional
public class AuteurServiceImpl implements AuteurService {

    @Autowired
    private AuteurRepository repository;

    @Override
    public List<Auteur> getAll() {
        return repository.findAll();
    }

    @Override
    public Auteur saveOrUpdate(Auteur auteur) {
        return repository.saveAndFlush(auteur);
    }

}
