package fr.dawan.springweb.services.impl;

import java.io.File;
import java.io.StringWriter;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import fr.dawan.springweb.services.EmailService;
import freemarker.template.Configuration;
import jakarta.mail.Message.RecipientType;
import jakarta.mail.internet.InternetAddress;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender sender;

    @Autowired
    private Configuration configuration;

    @Override
    public void sendSimpleMail(String content, String title, String to, String from) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setFrom(from);
        message.setSubject(title);
        message.setText(content);
        sender.send(message);
    }

    @Override
    public void sendHTMLMail(String content, String title, String to, String from) {
        sender.send(mimeMessage -> {
            mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(to));
            mimeMessage.setFrom(from);
            mimeMessage.setSubject(title, "utf-8");
            mimeMessage.setText(content, "utf-8", "html");
        });
    }

    @Override
    public void sendTempleMail(String template, Map<String, Object> modelMap, String titre, String to, String from)
            throws Exception {
        StringWriter sw = new StringWriter();
        configuration.getTemplate(template).process(modelMap, sw);
        sendHTMLMail(sw.toString(), titre, to, from);
    }

    @Override
    public void sendMailAttachement(String content, String title, String to, String from, File file) {
        sender.send(mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "utf-8");
            message.setTo(to);
            message.setFrom(from);
            message.setSubject(title);
            message.setText(content);
            message.addAttachment(file.getName(), file);
        });
    }

}
