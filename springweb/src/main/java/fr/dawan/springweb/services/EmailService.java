package fr.dawan.springweb.services;

import java.io.File;
import java.util.Map;

public interface EmailService {
    
    void sendSimpleMail(String content,String title,String to, String from);

    void sendHTMLMail(String content,String title,String to, String from);
    
    void sendTempleMail(String template,Map<String,Object> modelMap,String titre,String to, String from) throws Exception;

    void sendMailAttachement(String content,String title,String to, String from,File file);
}
