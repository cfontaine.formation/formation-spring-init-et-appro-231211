package fr.dawan.springweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springweb.entities.User;
import fr.dawan.springweb.repositories.UserRepository;
import fr.dawan.springweb.services.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;
    
    @Override
    public List<User> getAll() {
        return repository.findAll();
    }

    @Override
    public User saveOrUpdate(User user) {
        return repository.saveAndFlush(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.findByUsername(username)
                .orElseThrow(()->new UsernameNotFoundException("L'utilisateur n'existe pas"));
    }

}
