package fr.dawan.springweb.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.springweb.entities.Livre;

public interface LivreService {

    List<Livre> getAll(Pageable page);
    
    boolean deleteById(long id);
    
    Livre saveOrUpdate(Livre livre);

    Livre findById(long id);
}
