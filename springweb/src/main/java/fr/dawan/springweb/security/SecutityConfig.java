package fr.dawan.springweb.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecutityConfig {

    @Bean
    PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/admin/**").authenticated().anyRequest().permitAll())
                // .formLogin(Customizer.withDefaults());
                .formLogin((formLogin) -> formLogin.loginPage("/login")
                                                   .failureUrl("/login?failed=true")
                                                   .defaultSuccessUrl("/exemple",false))
                .logout((logout) -> logout.deleteCookies("JSESSIONID")
                                          .invalidateHttpSession(true));
        return http.build();
    }
}
